//
//  MOLPayExampleViewController.h
//  MOLPaySDK Example
//
//  Created by MOLPay on 5/16/14.
//  Copyright (c) 2014 MOLPay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOLPayExampleViewController : UIViewController
- (IBAction)payButton:(id)sender;

@end
